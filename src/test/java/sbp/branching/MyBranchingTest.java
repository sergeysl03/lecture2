package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTest
{
    @Test
    public void ifElseExample_Success_Test()
    {

        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(true);

        MyBranching myBranching = new MyBranching(utilsForTest);
        boolean result = myBranching.ifElseExample();
        Assertions.assertTrue(result);
    }
    @Test
    public void ifElseExample_Fall_Test()
    {

        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsForTest);
        boolean result = myBranching.ifElseExample();
        Assertions.assertFalse(result);
    }

    @Test
    public void MyBranching()

    {
        int i1 = 1, i2 = 2;
        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);

        MyBranching myBranching = new MyBranching(utilsForTest);
        int result = myBranching.maxInt( i1, i2 );
        Assertions.assertEquals(i2, result);
        Mockito.verify(utilsForTest, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsForTest, Mockito.never()).utilFunc1(Mockito.anyString());

    }


   @Test
    public void switchExample()
    {
        int i1 = 1;
        int i2 = 2;
        Utils utilsForTest = Mockito.mock(Utils.class);
        Mockito.when(utilsForTest.utilFunc1(anyString())).thenReturn(true);
        MyBranching myBranching = new MyBranching (utilsForTest);
        myBranching.switchExample(i1);

        Mockito.when(utilsForTest.utilFunc2()).thenReturn(false);
        myBranching = new MyBranching(utilsForTest);
        myBranching.switchExample(i2);

   }

}
